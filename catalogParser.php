<?php

$start = microtime(true);

ini_set('display_errors', 1);
error_reporting(E_ALL);

set_time_limit(0);
ignore_user_abort(true);

require_once('core/config/config.inc.php');

$dsn = "mysql:host=$database_server;dbname=$dbase;charset=$database_connection_charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => true,
];
$pdo = new PDO($dsn, $database_user, $database_password, $opt);

$settings = getConfig($pdo);

$emptySettings = array();

foreach($settings as $k => $v) {
    if(empty($v)) {
        if($k == 'maxId' || $k == 'maxId-2') continue;
        $emptySettings[] = $k;
    }
}
if(!empty($emptySettings)) {
    $counter = 1;
    echo '<meta charset="utf-8">';
    echo "Для работы скрипта, небходимо заполнить настройки: <br>";
    foreach($emptySettings as $setting) {
        echo $counter . '. ' . $setting . "<br>";
        $counter++;
    }
    return;
}else{
   foreach($settings as $k => $v) {
       $$k = $v;
   } 
}

//Функция получения настроек из чистемных настроек модыкса
function getConfig($pdo) {
    $sql = "SELECT * FROM  `modx_system_settings` WHERE `namespace` = 'catalogparser'";
    $stm = $pdo->query($sql);
    $row = $stm->fetchAll();
    
    $out = array();
    foreach($row as $k => $v) {
        $out[$v['key']] = $v['value'];
    }
    return $out;
}

//Функция получения потомков рекурсивно
function getChildIds($pdo, $root) {
    $st = $pdo->prepare("
        SELECT id
        FROM modx_site_content
        WHERE parent = :parent
    ");
    $st->execute(array('parent' => $root));
    $resultIds = array();
    if ($st) {
        while ($row = $st->fetch(\PDO::FETCH_ASSOC)) {
            $resultIds[] = $row['id'];
            $resultIds = array_merge($resultIds, getChildIds($pdo, $row['id']));
        }
    }
    return $resultIds;
}

//Функция очистки каталога
function clearCatalog($pdo, $rootId = false) {

    $idsArr = getChildIds($pdo, $rootId);
    if(count($idsArr) > 0) {
        $in = str_repeat('?,', count($idsArr) - 1) . '?';
        $st = $pdo->prepare("
            DELETE FROM modx_site_content, modx_site_tmplvar_contentvalues USING modx_site_content
    
            LEFT JOIN modx_site_tmplvar_contentvalues
            ON modx_site_tmplvar_contentvalues.contentid = modx_site_content.id
    
            WHERE modx_site_content.id IN ($in)
        ");
        $st->execute($idsArr);
    }
}
//Получаем максимальный id у ресурсов на сайте, чтобы в дальнейшем сложить его с получаемым id
function getMaxId($pdo) {
    $stmt = $pdo->query('SELECT max(id) as count FROM modx_site_content');
    $row = $stmt->fetch();
    return $row['count'];
}

//ПОлучаем контент и декдоируем его из json в формат php
function getContent() {
    $url = 'https://mebeloptom.com/export/?format=json&city=msk';
    $content = json_decode(file_get_contents($url), true);
    return $content;
}

function getColor($colArray, $idSearch) {
    $color = array();
    foreach($colArray as $k => $v) {
        if($v['id'] == $idSearch) {
            $color['picture'] = $v['picture'];
            $color['name'] = $v['name'];
            return $color;
        }
    }
}

//Обрабатываем параметры из массива в виде таблицы
function getChars($params) {
    $charsTable = '<table><tbody>';
    foreach($params as $k => $v) {
        $charsTable .= '<tr><td>' . $v['name'] . '</td><td>' . $v['value'] . '</td></tr>';
    }
    $charsTable .= '</tbody></table>';
    return $charsTable;
}

function getOptions($arr) {
    if($arr != NULL) {
        $out = array();
        
        foreach($arr as $k => &$v) {
            $migxArr = array();
            $migxId = $k + 1;
            $migxArr['MIGX_ID'] = "$migxId";
            $out[] = $migxArr + $v;
            
            if($out[$k]['order'] === 1) {
                $out[$k]['order'] = 'На заказ';
            }else{
                $out[$k]['order'] = 'Складская программа';
            }
        }
        
        return json_encode($out, JSON_UNESCAPED_UNICODE);
    }
}

function getModules($arr, $maxId) {
    if($arr != NULL) {
        $out = array();
        
        foreach($arr as $k => $v) {
            $migxArr = array();
            $migxId = $k + 1;
            $migxArr['MIGX_ID'] = "$migxId";
            $migxArr['id'] = $v['id'] + $maxId;
            $migxArr['position'] = $v['position'];
            
            if(!empty($v['submodules'])) {
                foreach($v['submodules'] as &$item) {
                    $item = $item + $maxId;
                }
                $submodules = $v['submodules'];
            }else{
                $submodules = '';
            }
            
            $migxArr['submodules'] = !empty($submodules) ? implode(',', $submodules) : '';
            $migxArr['quantity'] = $v['quantity'];
            
            $out[] = $migxArr;
        }
    }
    
    return json_encode($out, JSON_UNESCAPED_UNICODE);
}

function arrIdsToString($modules, $maxId) {
    $prepArr = array();
    
    foreach($modules as $k => $v) {
        $prepArr[] = $v['id'] + $maxId;
    }
    
    $out = implode(',', $prepArr);
    
    return $out;
}

//Проверяем, существует ли имя продукта уже в базе у текущей категории
function isNameExists($pdo, $name, $parent) {
    $sql = "SELECT id,pagetitle FROM modx_site_content WHERE pagetitle LIKE '%" . $name . "%' AND parent = $parent ORDER BY id ASC";
    $stm = $pdo->query($sql);
    $res = $stm->fetch();
    $stm->closeCursor();
    
    return $res;
}

//Поулчаем tpl с окончанием загрузки
function displayLoad() {
    return file_get_contents('template.tpl');
}

if(isset($_GET['mode'])) {
    $mode = $_GET['mode'];
}

$content = getContent();
$products = $content['catalog']['products'];
$colors = $content['catalog']['colors'];

if(isset($_GET['mode'])) {
    $mode = $_GET['mode'];
}
switch($mode) {
    case 'products':
        
        clearCatalog($pdo, $catalogRes);
        clearCatalog($pdo, $moduleRes);
        
        //Сбрасываем автоинкремент
        $increment = getMaxId($pdo) + 1;
        
        $sql = 'ALTER TABLE modx_site_content AUTO_INCREMENT = ' . $increment;
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $stm->closeCursor();
        
        $maxId = getMaxId($pdo);
        $sql = "UPDATE `modx_system_settings` SET `value` = $maxId WHERE `key` = 'maxId'";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $stm->closeCursor();
        
        
        
        $categories = $content['catalog']['categories'];
        file_put_contents('contentlog.txt', date('d-m-Y H:i:s') . "\n", FILE_APPEND | LOCK_EX);

        //Добавление категорий
        foreach($content['catalog']['categories'] as $k => $v) {
            $id = $v['id'] + $maxId;
            $pagetitle = $v['name'];
            $alias = $v['name'] . '-' . $v['id'];
            $parent = isset($v['parent_id']) ? $v['parent_id'] + $maxId : $catalogRes;
            $time = time();
            $sql = "INSERT INTO modx_site_content SET id = $id, pagetitle = '$pagetitle', alias = '$alias', description = '', parent = $parent, published = 1, publishedon = $time, show_in_tree = 1, template = $catTemplate, class_key = 'CollectionContainer'";
            $stm = $pdo->query($sql);
            $stm->closeCursor();
            
            $log =  "Категория " . $v['name'] . "(" . $v['id'] . ")" . " добавлен к родителю " . $parent . "\n";
            file_put_contents('contentlog.txt', $log, FILE_APPEND | LOCK_EX);
        }
        
        //Добавление товаров
        foreach($products as $k => $v) {
            
            $colorArr = getColor($colors, $v['color_id']);    
            $colorPic = $colorArr['picture'];
            $colorName = $colorArr['name'];
            
            $id = $v['id'] + $maxId;
            $pagetitle = $v['name'] . " - " . $colorName;
            $longtitle = $v['name'];
            $parent = $v['category_id'] + $maxId;
            
            //$alias = trim(strrchr($v['url'], '/' ), '/');
            $url = preg_match('/.com\/(.*)/i', $v['url'], $matches);
            $alias = $matches[1];
            
            $isNameExists = isNameExists($pdo, $longtitle, $parent);
            if($isNameExists != false) {
                $parent = $isNameExists['id'];
            }
        
            $time = time();
            $price = $v['price_rrc'];
            $image = $v['pictures'][0];
            $chars = getChars($v['params']);
            $options = getOptions($v['options']);
            
            $sql = "INSERT INTO modx_site_content SET id = $id, pagetitle = '$pagetitle', longtitle = '$longtitle', description = '', alias = '$alias', parent = $parent, published = 1, publishedon = $time, show_in_tree = 1, template = $prodTemplate;
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $priceTv, contentid = $id, value = $price;
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $colorTv, contentid = $id, value = '$colorPic';
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $imageTv, contentid = $id, value = '$image';
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $charsTv, contentid = $id, value = '$chars';
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $optionsTv, contentid = $id, value = '$options';
            ";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $stm->closeCursor();
        
            $log =  "Товар " . $v['name'] . "(" . $v['id'] . ")" . " добавлен к родителю " . $parent . " | Цена: " . $price . "\n";
            file_put_contents('contentlog.txt', $log, FILE_APPEND | LOCK_EX);
        }
        
        $newMaxId = getMaxId($pdo);
        
        foreach($products as $k => $v) {
            
            $id = $v['id'] + $maxId;
            
            $modules = !empty($v['modules']) ? getModules($v['modules'], $newMaxId) : '';
            
            $sql = "INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $modulesTv, contentid = $id, value = '$modules'";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $stm->closeCursor();
        }
        break;
        
    case 'modules':
        
        $part = (($_GET['part'] == 1 || $_GET['part'] == 2 || $_GET['part'] == 3)) ? $_GET['part'] : '';
        
        switch($part) {
            case 1:
                $newMaxId = getMaxId($pdo);
                
                $sql = "UPDATE `modx_system_settings` SET `value` = $newMaxId WHERE `key` = 'maxId-2'";
                $stm = $pdo->prepare($sql);
                $stm->execute();
                $stm->closeCursor();
                
                $items = 'first';
                break;
                
            case 2:
                
                $sql = "SELECT `value` FROM `modx_system_settings` WHERE `key` = 'maxId'";
                $stm = $pdo->query($sql);
                $res = $stm->fetch();
                $stm->closeCursor();
                
                $maxId = $res['value'];
                
                $sql = "SELECT `value` FROM `modx_system_settings` WHERE `key` = 'maxId-2'";
                $stm = $pdo->query($sql);
                $res = $stm->fetch();
                $stm->closeCursor();
                
                $newMaxId = $res['value'];
                
                $items = 'second';
                break;
                
            case 3:
                
                $sql = "SELECT `value` FROM `modx_system_settings` WHERE `key` = 'maxId'";
                $stm = $pdo->query($sql);
                $res = $stm->fetch();
                $stm->closeCursor();
                
                $maxId = $res['value'];
                
                $sql = "SELECT `value` FROM `modx_system_settings` WHERE `key` = 'maxId-2'";
                $stm = $pdo->query($sql);
                $res = $stm->fetch();
                $stm->closeCursor();
                
                $newMaxId = $res['value'];
                
                $items = 'third';
                break;
                
            default:
                return;
        }
        
        $modules = $content['catalog']['modules'];
        
        list($first, $second, $third) = array_chunk($modules, ceil(count($modules)/3));
        
        //Добавление модулей
        foreach($$items as $k => $v) {
            
            $colorArr = getColor($colors, $v['color_id']);
            $colorPic = $colorArr['picture'];
            $colorName = $colorArr['name'];
            
            $id = $v['id'] + $newMaxId;
            $pagetitle = $v['name'];
            $parent = $moduleRes;
            $alias = 'catalog/module-' . $id;
            
            $isNameExists = isNameExists($pdo, $pagetitle, $parent);
            if($isNameExists != false) {
                $parent = $isNameExists['id'];
            }
            
            $time = time();
            $price = $v['price_rrc'];
            $image = !empty($v['pictures'][0]) ? $v['pictures'][0] : '';
            $chars = getChars($v['params']);
            $modCategories = !empty($v['categories']) ? arrIdsToString($v['categories'], $maxId) : '';
            
            $sql = "INSERT INTO modx_site_content SET id = $id, pagetitle = '$pagetitle', description = '', alias = '$alias', parent = $parent, published = 1, publishedon = $time, show_in_tree = 0, template = $moduleTemplate;
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $priceTv, contentid = $id, value = $price;
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $colorTv, contentid = $id, value = '$colorPic';
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $imageTv, contentid = $id, value = '$image';
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $charsTv, contentid = $id, value = '$chars';
                    INSERT INTO modx_site_tmplvar_contentvalues SET tmplvarid = $categoriesTv, contentid = $id, value = '$modCategories'
            ";
            $stm = $pdo->prepare($sql);
            $stm->execute();
            $stm->closeCursor();
            
            $log =  "Модуль " . $v['name'] . "(" . $v['id'] . ")" . " добавлен к родителю " . $parent . " | Цена: " . $price . "\n";
            file_put_contents('contentlog.txt', $log, FILE_APPEND | LOCK_EX);
        }
        break;
        
    default:
        return;
}

$time = microtime(true) - $start;
echo $time;